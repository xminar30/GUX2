tetris : tetris.o
	$(CC) $(LDFLAGS) -g -o $@ $^ `pkg-config --libs gtk+-3.0` 

%.o : %.c
	$(CC) $(CFLAGS) -c -O2 -g -Wall -DGTK_DISABLE_DEPRECATED=1 -DGDK_DISABLE_DEPRECATED=1 -DG_DISABLE_DEPRECATED=1 `pkg-config --cflags gtk+-3.0` $^

clean:
	rm -f *.o tetris
