// tetris.c
// autor: Antonin Minarik (xminar30)
// datum: prosinec 2017

#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>

// rozmery herniho pole
#define FIELD_WIDTH 10
#define FIELD_HEIGHT 14

// makro pro pseudonahodne generovani
#define RAND ((double)rand()/((double)RAND_MAX+1.))

// typy tetris tvaru
typedef enum {
SHAPE_SQUARE = 0,
SHAPE_LONG,
SHAPE_L,
SHAPE_RL,
SHAPE_S,
SHAPE_RS,
SHAPE_T,
NUM_OF_SHAPES
} TETRIS_SHAPE;

// struktura pro tetris tvar
typedef struct {
char start_shift;
char start_fall;
char shape[4][2];
} tShape;

// definice tvaru a jejich rotaci
#include "shape_definitions.h"

// barvy
#define NUM_OF_COLORS 6
const double colors[][3] = {
{1, 1, 1},
{0.6, 0, 0},
{0, 0.6, 0},
{0, 0, 0.6},
{0.6, 0.6, 0},
{0.6, 0, 0.6},
{0, 0.6, 0.6},
};

// plocha pro vykresleni tetrisu
GtkWidget *drawing_area;
GtkWidget *drawing_next_shape;
GtkWidget *score_label;
static cairo_surface_t *game_surface = NULL;
static cairo_surface_t *next_shape_surface = NULL;

// pole zaznamenavajici stav tetrisu
unsigned char game_field[FIELD_WIDTH][FIELD_HEIGHT];

// hodnota skore
int score = 0;

// bezi hra?
char game_running = 0;

// informace o nasledujicim tvaru
TETRIS_SHAPE next_shape;
unsigned char next_rotation;
unsigned char next_falling_color;

// informace o aktualnim tvaru
TETRIS_SHAPE current_shape;
unsigned char current_rotation;
char falling_shape[4][2];
char fallen_distance;
char falling_shift;
char shape_is_falling = 0;
unsigned char falling_color;

// otestuje zda je mozna rotace,
// jestli ano provede ji
void try_rotate()
{
	int stop;
	char new_shape[4][2];
	int i, j;
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 2; j++) {
			new_shape[i][j] =
				shapes[current_shape][(current_rotation+1)%4]->shape[i][j];
		}
	}

	for (i = 0; i < 4; i++) {
		stop = 0;
		int fieldx = new_shape[i][0]+falling_shift;
		int fieldy = new_shape[i][1]+fallen_distance;
		if (fieldy > FIELD_HEIGHT-1
				|| fieldy < 0
				|| fieldx < 0
				|| fieldx > FIELD_WIDTH-1
				|| game_field[fieldx][fieldy] > 0) {
			stop++;
			break;
		}
	}
	if (!stop) {
		int i, j;
		for (i = 0; i < 4; i++) {
			for (j = 0; j < 2; j++) {
				falling_shape[i][j] = new_shape[i][j];
			}
		}
		current_rotation = (current_rotation+1)%4;
	}
}

// otestuje zda je mozny pohyb padajiciho tvaru
// dx posun na ose x
// dy posun na ose y
int can_move(int dx, int dy)
{
	int i;
	int stop;
	for (i = 0; i < 4; i++) {
		stop = 0;
		int fieldx = falling_shape[i][0]+falling_shift+dx;
		int fieldy = falling_shape[i][1]+fallen_distance+dy;
		if (fieldy > FIELD_HEIGHT-1
				|| fieldx < 0
				|| fieldx > FIELD_WIDTH-1
				|| game_field[fieldx][fieldy] > 0) {
			stop++;
			break;
		}
	}
	return !stop;
}

// vykresli padajici tvar
void draw_falling(cairo_t *cr)
{
	int i;
	for (i = 0; i < 4; i++) {
		cairo_set_source_rgb (cr,
				colors[falling_color][0],
				colors[falling_color][1],
				colors[falling_color][2]);
		cairo_rectangle(cr,
				(falling_shape[i][0]+falling_shift)*40,
				(falling_shape[i][1]+fallen_distance)*40,
				40, 40);
		cairo_fill(cr);
	}
}

// vypise hlasku o konci hry
void show_game_over() {
	cairo_t *cr;

	cr = cairo_create(game_surface);

	cairo_set_source_rgb (cr, 0, 0, 0);
	cairo_rectangle(cr, 60, 150, 280, 120);
	cairo_fill(cr);

	cairo_set_source_rgb (cr, 0, 1, 0);
	cairo_select_font_face(cr, "cairo",
			CAIRO_FONT_SLANT_NORMAL,
			CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_font_size(cr, 40);

	cairo_move_to(cr, 75, 220);
	cairo_show_text(cr, "Game Over");

	cairo_destroy(cr);

	gtk_widget_queue_draw_area(drawing_area, 0, 0, 400, 560);
}

// prekresli hru 
void game_redraw()
{
	cairo_t *cr;
	int i, j;

	cr = cairo_create(game_surface);

	for (i = 0; i < FIELD_WIDTH; i++) {
		for (j = 0; j < FIELD_HEIGHT; j++) {
			cairo_set_source_rgb (cr,
					colors[game_field[i][j]][0],
					colors[game_field[i][j]][1],
					colors[game_field[i][j]][2]);
			cairo_rectangle(cr, i*40, j*40, 40, 40);
			cairo_fill(cr);
		}
	}

	if (shape_is_falling == 1)
		draw_falling(cr);

	if (!game_running)
		show_game_over();

	cairo_destroy(cr);

	gtk_widget_queue_draw_area(drawing_area, 0, 0, 400, 560);
}

// prekresli dalsi padajici tvar
void next_shape_redraw()
{
	cairo_t *cr;
	int i;

	cr = cairo_create(next_shape_surface);

	cairo_set_source_rgb (cr, 1, 1, 1);
	cairo_rectangle(cr, 0, 0, 120, 120);
	cairo_fill(cr);

	cairo_set_source_rgb (cr,
			colors[next_falling_color][0],
			colors[next_falling_color][1],
			colors[next_falling_color][2]);

	for (i = 0; i < 4; i++) {
		cairo_rectangle(cr,
				(shapes[next_shape][next_rotation]->shape[i][0]
				+ shapes[next_shape][next_rotation]->start_shift - 3)*30,
				(shapes[next_shape][next_rotation]->shape[i][1]
				+ shapes[next_shape][next_rotation]->start_fall)*30,
				30, 30);
		cairo_fill(cr);
	}

	cairo_destroy(cr);

	gtk_widget_queue_draw_area(drawing_next_shape, 0, 0, 120, 120);
}

// posune herni plochu
void one_line_fall(int to)
{
	int i;
	for (i = to-1; i >= 0; i--) {
		int j;
		for (j = 0; j < FIELD_WIDTH; j++) {
			game_field[j][i+1] = game_field[j][i];
		}
	}
}

// nastavi zadane skore do labelu
void set_score(int s)
{
	char lbl_text[100];
	sprintf(lbl_text, "<span font='30'>score: %d</span>", s*10);
	gtk_label_set_text(GTK_LABEL(score_label), lbl_text);
	gtk_label_set_use_markup(GTK_LABEL(score_label), TRUE);
}

// zkontroluje vsechny plne radky,
// a vymaze je
void check_full_lines()
{
	int i;
	int full;
	for (i = FIELD_HEIGHT-1; i >= 0; i--) {
		full = 0;
		int j;
		for (j = 0; j < FIELD_WIDTH; j++) {
			full += (game_field[j][i] != 0);
		}
		if (full == FIELD_WIDTH) {
			one_line_fall(i);
			i++;
			score += 1;
			set_score(score);
			continue;
		}
	}
}

// zkontroluje moznost padu padajiciho tvaru o dalsi policko,
// kdyz je to mozne posune ho
void fall_one()
{
	int i;

	if (can_move(0, 1))
		fallen_distance++;
	else {
		for (i = 0; i < 4; i++) {
			int fieldx = falling_shape[i][0]+falling_shift;
			int fieldy = falling_shape[i][1]+fallen_distance;
			game_field[fieldx][fieldy] = falling_color;
		}
		shape_is_falling = 0;
	}
}

// vygeneruje dalsi padajici tvar
void generate_new_shape()
{
	current_shape = next_shape;
	current_rotation = next_rotation;
	falling_color = next_falling_color;

	next_shape = RAND*NUM_OF_SHAPES;
	next_rotation = RAND*4;
	next_falling_color = (RAND*NUM_OF_COLORS)+1;
	int i, j;

	for (i = 0; i < 4; i++) {
		for (j = 0; j < 2; j++) {
			falling_shape[i][j] =
				shapes[current_shape][current_rotation]->shape[i][j];
		}
	}
	falling_shift = shapes[current_shape][current_rotation]->start_shift;
	fallen_distance = shapes[current_shape][current_rotation]->start_fall;
	if (!can_move(0, 0)) {
		game_running = 0;
	}
}

// zkontroluje zda je treba generovat dalsi tvar
void check_new_shape()
{
	if (shape_is_falling == 0) {
		generate_new_shape();
		shape_is_falling = 1;
	}
}

// pokud hra bezi, provede dalsi krok tetrisu
gint tetris_step(gpointer data)
{
	if (!game_running) return FALSE;


	fall_one();

	check_full_lines();

	check_new_shape();

	game_redraw();

	next_shape_redraw();

	return TRUE;
}

void clear_game_field()
{
	int i, j;
	for (i = 0; i < FIELD_WIDTH; i++) {
		for (j = 0; j < FIELD_HEIGHT; j++) {
			game_field[i][j] = 0;
		}
	}
}

// prepne panel do menu
static void stack_main_menu(GtkWidget *widget, gpointer data)
{
	game_running = 0;
	gtk_stack_set_visible_child_name(GTK_STACK(data), "main_menu");
}

// prepne panel do hry a odstartuje ji
static void stack_new_game(GtkWidget *widget, gpointer data)
{
	gtk_stack_set_visible_child_name(GTK_STACK(data), "new_game");
	score = 0;
	set_score(score);
	generate_new_shape();
	shape_is_falling = 0;
	clear_game_field();
	game_running = 1;
	game_redraw();
	g_timeout_add(500, tetris_step, NULL);
}

// zobrazi panel s napovedou
static void stack_help(GtkWidget *widget, gpointer data)
{
	gtk_stack_set_visible_child_name(GTK_STACK(data), "help");
}

// vytvori novou plochu pro kresleni hry
static gboolean configure_event_cb(
		GtkWidget         *widget,
		GdkEventConfigure *event,
		gpointer           data)
{
	if (game_surface)
		cairo_surface_destroy(game_surface);

	game_surface = gdk_window_create_similar_surface(gtk_widget_get_window(widget),
			CAIRO_CONTENT_COLOR,
			gtk_widget_get_allocated_width(widget),
			gtk_widget_get_allocated_height(widget));

	game_redraw();

	return TRUE;
}

// vytvori novou plochu pro kresleni dalsiho tvaru
static gboolean configure_event_next_shape_cb(
		GtkWidget         *widget,
		GdkEventConfigure *event,
		gpointer           data)
{
	if (next_shape_surface)
		cairo_surface_destroy(next_shape_surface);

	next_shape_surface = gdk_window_create_similar_surface(gtk_widget_get_window(widget),
			CAIRO_CONTENT_COLOR,
			gtk_widget_get_allocated_width(widget),
			gtk_widget_get_allocated_height(widget));

	next_shape_redraw();

	return TRUE;
}

// prekresli herni plochu
static gboolean draw_cb(
		GtkWidget *widget,
		cairo_t   *cr,
		gpointer   data)
{
	cairo_set_source_surface(cr, game_surface, 0, 0);
	cairo_paint(cr);

	return FALSE;
}

// prekresli plochu dalsiho tvaru
static gboolean draw_next_shape_cb(
		GtkWidget *widget,
		cairo_t   *cr,
		gpointer   data)
{
	cairo_set_source_surface(cr, next_shape_surface, 0, 0);
	cairo_paint(cr);

	return FALSE;
}

// zrusi kreslici plochy
static void close_window(void)
{
	if (game_surface)
		cairo_surface_destroy(game_surface);

	if (next_shape_surface)
		cairo_surface_destroy(next_shape_surface);
}

// akce pri stisku klaves
static gboolean key_press_event_cb(
		GtkWidget      *widget,
		GdkEventKey    *event,
		gpointer        data)
{
	if (game_running) {
		switch (event->keyval) {
			case GDK_KEY_Up:
				try_rotate();
				break;
			case GDK_KEY_Down:
				while (shape_is_falling) {
					fall_one();
				}
				break;
			case GDK_KEY_Left:
				if (can_move(-1, 0))
					falling_shift--;
				break;
			case GDK_KEY_Right:
				if (can_move(1, 0))
					falling_shift++;
				break;
		}
		game_redraw();
	}
	return TRUE;
}

// priprava a aktivace celeho GUI
static void activate (GtkApplication *app, gpointer user_data)
{
	GtkWidget *window;
	GtkWidget *button;
	GtkWidget *main_menu;
	GtkWidget *new_game;
	GtkWidget *help;
	GtkWidget *help_label;
	GtkWidget *layout;
	GtkWidget *stack_switcher;
	GtkWidget *stack;
	GtkWidget *button_box;
	GtkWidget *frame;
	GtkWidget *box;

	// hlavni okno
	window = gtk_application_window_new(app);
	gtk_window_set_title(GTK_WINDOW(window), "Tetris");
	gtk_window_set_default_size(GTK_WINDOW(window), 800, 600);
	gtk_container_set_border_width(GTK_CONTAINER(window), 10);

	// stack pro umisteni panelu
	stack = gtk_stack_new();

	layout = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
	gtk_container_add(GTK_CONTAINER(window), layout);

	stack_switcher = gtk_stack_switcher_new();
	gtk_stack_switcher_set_stack(GTK_STACK_SWITCHER(stack_switcher),GTK_STACK(stack));

	// hlavni menu
	main_menu = gtk_button_box_new(GTK_ORIENTATION_VERTICAL);
	gtk_box_set_spacing(GTK_BOX(main_menu), 15);
	gtk_button_box_set_layout(GTK_BUTTON_BOX(main_menu), GTK_BUTTONBOX_CENTER);

	// tlacitko pro novou hru
	button = gtk_button_new_with_label("<span font='20'>New Game</span>");
	gtk_label_set_use_markup(
			GTK_LABEL(gtk_container_get_children(GTK_CONTAINER(button))->data),
			TRUE);
	g_signal_connect(button, "clicked", G_CALLBACK(stack_new_game), stack);
	gtk_container_add(GTK_CONTAINER(main_menu), button);

	// tlacitko napovedy
	button = gtk_button_new_with_label("<span font='20'>Help</span>");
	gtk_label_set_use_markup(
			GTK_LABEL(gtk_container_get_children(GTK_CONTAINER(button))->data),
			TRUE);
	g_signal_connect(button, "clicked", G_CALLBACK(stack_help), stack);
	gtk_container_add(GTK_CONTAINER(main_menu), button);

	// tlacitko pro konec
	button = gtk_button_new_with_label("<span font='20'>Quit</span>");
	gtk_label_set_use_markup(
			GTK_LABEL(gtk_container_get_children(GTK_CONTAINER(button))->data),
			TRUE);
	g_signal_connect_swapped(button, "clicked", G_CALLBACK(gtk_widget_destroy), window);
	gtk_container_add(GTK_CONTAINER(main_menu), button);

	// tlacitko pro navrat do hlavniho menu
	button_box = gtk_button_box_new(GTK_ORIENTATION_VERTICAL);
	gtk_button_box_set_layout(GTK_BUTTON_BOX(button_box), GTK_BUTTONBOX_END);

	button = gtk_button_new_with_label("<span font='20'>Back</span>");
	gtk_label_set_use_markup(
			GTK_LABEL(gtk_container_get_children(GTK_CONTAINER(button))->data),
			TRUE);
	g_signal_connect(button, "clicked", G_CALLBACK(stack_main_menu), stack);
	gtk_container_add(GTK_CONTAINER(button_box), button);

	new_game = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 20);
	gtk_box_pack_start(GTK_BOX(new_game), button_box, FALSE, FALSE, 0);

	// napoveda
	help = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 20);
	button_box = gtk_button_box_new(GTK_ORIENTATION_VERTICAL);
	gtk_button_box_set_layout(GTK_BUTTON_BOX(button_box), GTK_BUTTONBOX_END);

	help_label = gtk_label_new("<span font='30'>Controls:\n"
			"Left/Right arrow - moving with shape\n"
			"Up arrow - shape rotations\n"
			"Down arrow - immediate fall</span>");
	gtk_label_set_use_markup(GTK_LABEL(help_label), TRUE);
	gtk_box_set_center_widget(GTK_BOX(help), help_label);

	// tlacitko pro navrat z napovedy
	button = gtk_button_new_with_label("<span font='20'>Back</span>");
	gtk_label_set_use_markup(
			GTK_LABEL(gtk_container_get_children(GTK_CONTAINER(button))->data),
			TRUE);
	g_signal_connect(button, "clicked", G_CALLBACK(stack_main_menu), stack);
	gtk_container_add(GTK_CONTAINER(button_box), button);
	gtk_box_pack_start(GTK_BOX(help), button_box, FALSE, FALSE, 0);


	// pridani do stacku pro moznost prepinani
	gtk_stack_add_named(GTK_STACK(stack), main_menu, "main_menu");
	gtk_stack_add_named(GTK_STACK(stack), new_game, "new_game");
	gtk_stack_add_named(GTK_STACK(stack), help, "help");


	g_signal_connect(window, "destroy", G_CALLBACK(close_window), NULL);

	// ramecek pro herni plochu
	frame = gtk_aspect_frame_new(NULL, 0, 0, 0.6f, TRUE);
	gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);

	gtk_box_set_center_widget(GTK_BOX(new_game), frame);

	// herni plocha
	drawing_area = gtk_drawing_area_new();

	// velikost herni plochy
	gtk_widget_set_size_request(drawing_area, 400, 560);

	gtk_container_add(GTK_CONTAINER(frame), drawing_area);

	// udalosti pro kresleni a vytvoreni herni plochy
	g_signal_connect(drawing_area, "draw",
			G_CALLBACK(draw_cb), NULL);
	g_signal_connect(drawing_area,"configure-event",
			G_CALLBACK(configure_event_cb), NULL);

	box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
	gtk_box_pack_end(GTK_BOX(new_game), box, FALSE, FALSE, 0);

	// ramecek pro zobrazeni dalsiho tvaru
	frame = gtk_aspect_frame_new(NULL, 0, 0, 0.6f, TRUE);
	gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);

	gtk_box_pack_start(GTK_BOX(box), frame, FALSE, FALSE, 0);


	// label pro skore
	score_label = gtk_label_new("<span font='30'>score: 0</span>");
	gtk_label_set_use_markup(GTK_LABEL(score_label), TRUE);
	gtk_widget_set_size_request(GTK_WIDGET(score_label), 140, 100);
	gtk_box_pack_start(GTK_BOX(box), score_label, FALSE, FALSE, 0);

	// plocha pro zoprazeni dalsiho tvaru
	drawing_next_shape = gtk_drawing_area_new();

	// velikost plochy pro zobrazeni dalsiho tvaru
	gtk_widget_set_size_request(drawing_next_shape, 120, 120);

	gtk_container_add(GTK_CONTAINER(frame), drawing_next_shape);

	// udalosti pro kresleni a vytvoreni plochy pro dalsi tvar
	g_signal_connect(drawing_next_shape, "draw",
			G_CALLBACK(draw_next_shape_cb), NULL);
	g_signal_connect(drawing_next_shape,"configure-event",
			G_CALLBACK(configure_event_next_shape_cb), NULL);

	// obsluha stisku klavesy
	g_signal_connect (G_OBJECT(window),
			"key-press-event",
			G_CALLBACK(key_press_event_cb), NULL);

	// pridani stacku
	gtk_box_pack_start(GTK_BOX(layout), stack_switcher, FALSE, FALSE, 0);
	gtk_box_pack_end(GTK_BOX(layout), stack, TRUE, TRUE, 0);

	// vykresleni vseho
	gtk_widget_show_all(window);
}

// hlavni funkce
int main (int argc, char **argv)
{
	GtkApplication *app;
	int status;

	// seed pro pseudonahodne generovani tvaru
	srand(time(NULL));

	// vytvoreni aplikace
	app = gtk_application_new("gux2017.xminar30.tetris", G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
	status = g_application_run(G_APPLICATION(app), argc, argv);
	g_object_unref(app);

	return status;
}

// konec souboru tetris.c
