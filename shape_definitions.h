// shape_definitions.h
// autor: Antonin Minarik (xminar30)
// datum: prosinec 2017

const tShape shapeSquare = {
4, 0,
{{0, 0}, {0, 1}, {1, 0}, {1, 1}}
};

const tShape shapeLong1 = {
4, 0,
{{-1, 0}, {0, 0}, {1, 0}, {2, 0}}
};

const tShape shapeLong2 = {
4, 1,
{{1, -1}, {1, 0}, {1, 1}, {1, 2}}
};

const tShape shapeL1 = {
4, 1,
{{0, -1}, {0, 0}, {0, 1}, {1, 1}}
};

const tShape shapeL2 = {
4, 0,
{{-1, 1}, {0, 1}, {1, 1}, {1, 0}}
};

const tShape shapeL3 = {
4, 1,
{{0, -1}, {1, -1}, {1, 0}, {1, 1}}
};

const tShape shapeL4 = {
4, 0,
{{-1, 1}, {-1, 0}, {0, 0}, {1, 0}}
};

const tShape shapeRL1 = {
4, 1,
{{1, -1}, {1, 0}, {1, 1}, {0, 1}}
};

const tShape shapeRL2 = {
4, 0,
{{-1, 0}, {0, 0}, {1, 1}, {1, 0}}
};

const tShape shapeRL3 = {
4, 1,
{{0, -1}, {1, -1}, {0, 0}, {0, 1}}
};

const tShape shapeRL4 = {
4, 0,
{{-1, 1}, {-1, 0}, {0, 1}, {1, 1}}
};

const tShape shapeS1 = {
4, 1,
{{0, 0}, {0, 1}, {1, -1}, {1, 0}}
};

const tShape shapeS2 = {
4, 0,
{{-1, 0}, {0, 0}, {0, 1}, {1, 1}}
};

const tShape shapeRS1 = {
4, 1,
{{0, -1}, {0, 0}, {1, 0}, {1, 1}}
};

const tShape shapeRS2 = {
5, 0,
{{0, 0}, {1, 0}, {0, 1}, {-1, 1}}
};

const tShape shapeT1 = {
4, 0,
{{0, 0}, {-1, 0}, {1, 0}, {0, 1}}
};

const tShape shapeT2 = {
4, 1,
{{0, -1}, {0, 0}, {0, 1}, {1, 0}}
};

const tShape shapeT3 = {
4, 0,
{{0, 0}, {-1, 1}, {0, 1}, {1, 1}}
};

const tShape shapeT4 = {
4, 1,
{{0, -1}, {0, 0}, {-1, 0}, {0, 1}}
};


//adresace pomoci
//tvar, rotace
const tShape *shapes[NUM_OF_SHAPES][4] = {
//SHAPE_SQUARE
{
&shapeSquare,
&shapeSquare,
&shapeSquare,
&shapeSquare,
},
//SHAPE_LONG
{
&shapeLong1,
&shapeLong2,
&shapeLong1,
&shapeLong2,
},
//SHAPE_L
{
&shapeL1,
&shapeL2,
&shapeL3,
&shapeL4,
},
//SHAPE_RL
{
&shapeRL1,
&shapeRL2,
&shapeRL3,
&shapeRL4,
},
//SHAPE_S
{
&shapeS1,
&shapeS2,
&shapeS1,
&shapeS2,
},
//SHAPE_RS
{
&shapeRS1,
&shapeRS2,
&shapeRS1,
&shapeRS2,
},
//SHAPE_T
{
&shapeT1,
&shapeT2,
&shapeT3,
&shapeT4,
},
};

// konec souboru shape_definitions.h
